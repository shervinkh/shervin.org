import React from 'react'

export const NewPageLink = ({ to, children }) =>
  <a href={to} rel='noopener noreferrer' target='_blank'>{children}</a>
