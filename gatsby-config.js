module.exports = {
  siteMetadata: {
    title: 'Shervin Khastoo\'s Personal Webpage',
  },
  plugins: [
    'gatsby-plugin-antd',
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Shervin Khastoo\'s Personal Webpage',
        short_name: 'Shervin.org',
        start_url: '/',
        background_color: 'white',
        theme_color: '#2746e5',
        display: 'minimal-ui',
        icon: 'src/images/icon.png',
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    'gatsby-plugin-offline',
  ],
}
