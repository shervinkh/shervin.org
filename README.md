# Shervin Khastoo's Personal Webpage
This project is Shervin Khastoo's personal webpage using GatsbyJS. It is being deployed on [shervin.org](http://shervin.org).

## Development Guide
```sh
npm install -g gatsby-cli
npm install
gatsby develop
```
